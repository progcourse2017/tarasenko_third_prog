#include "stdafx.h"
#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	int rows;
	int columns;
	cout << "Введите количество строк массива" << endl;
	cin >> rows;
	cout << "Введите количство столбцов массива" << endl;
	cin >> columns;
	int **array = new int*[rows];
	for (int i=0; i<rows; i++) 
	{
		array[i] = new int[columns];
	}
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			array[i][j] = rand()%10;
		}
	}
	for (int i = 0; i < rows; i++)
	{
		for (int j = 0; j < columns; j++)
		{
			cout << array[i][j] << "\t";
		}
		cout << endl;	
	}
	system("pause");
	for (int i = 0; i < rows; i++)
	{
		delete[] array[i];
	}
	delete[] array;
    return 0;
}

